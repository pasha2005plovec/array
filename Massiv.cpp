﻿#include <iostream>
using std::cout;
#include <ctime>
#include <sstream>
#include <iomanip>
#include <windows.h>
	using namespace std;

    int main() {

        const int size = 5;
        int array[size][size];
        for (int i = 0; i < size; i++) 
        {
            for (int j = 0; j < size; j++) 
            {
                array[i][j] = i + j;
                cout << array[i][j] << " ";
            }
            cout << endl;
        }
        SYSTEMTIME st;
        GetSystemTime(&st);
        int day = st.wDay;
        int sum = 0;
        for (int x = 0; x < size; x++)
        {
            sum += array[day % size][x];
        }
        cout << sum << endl;

      
        return 0;
    }
